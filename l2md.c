// SPDX-License-Identifier: GPL-2.0-only
/* Copyright (C) 2019 Daniel Borkmann <daniel@iogearbox.net> */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <git2.h>

#include "l2md.h"

pid_t own_pid;

static volatile sig_atomic_t sigint;

static void signal_handler(int number)
{
	if (number == SIGINT)
		sigint = 1;
}

static  __attribute__((constructor)) void main_init(void)
{
	git_libgit2_init();
	own_pid = getpid();
}

static  __attribute__((destructor)) void main_exit(void)
{
	git_libgit2_shutdown();
}

int main(int argc, char **argv)
{
	struct config *cfg = config_init(argc, argv);

	bootstrap_env(cfg);
	signal(SIGINT, signal_handler);

	do {
		sync_mail(cfg);
		sync_done(cfg);
		if (sigint)
			break;
		sync_env(cfg);
	} while (!sigint && !cfg->oneshot);

	config_uninit(cfg);
	return 0;
}
