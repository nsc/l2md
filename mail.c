// SPDX-License-Identifier: GPL-2.0-only
/* Copyright (C) 2019 Daniel Borkmann <daniel@iogearbox.net> */

#include <unistd.h>

#include <sys/time.h>
#include <sys/types.h>

#include "l2md.h"

static void repo_walker(struct config *cfg, struct config_repo *repo,
			uint32_t url, const char *oid,
			const void *raw, size_t len)
{
	cfg->ops->new_mail(repo, url, oid, raw, len);
}

void sync_mail(struct config *cfg)
{
	struct config_repo *repo;
	struct config_url *url;
	char path[PATH_MAX];
	uint32_t i, j;

	verbose("Resyncing mails.\n");

	repo_for_each(cfg, repo, i) {
		url_for_each(repo, url, j) {
			repo_local_path(cfg, repo, url, path, sizeof(path));
			repo_walk_files(cfg, repo, j, path,
					url->oid_known ? url->oid : NULL,
					url->oid, repo_walker);

			repo_local_oid(cfg, repo, url, path, sizeof(path));
			xwrite_file(path, url->oid, sizeof(url->oid) - 1,
				    true);
			url->oid_known = true;
		}
	}
}
